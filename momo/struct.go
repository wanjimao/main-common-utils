package momo

//{"ec":210,"em":"app terminated","millisecond":1689137879762,"data":null}

//	type T struct {
//		Ec          int         `json:"ec"`
//		Em          string      `json:"em"`
//		Millisecond int64       `json:"millisecond"`
//		Data        interface{} `json:"data"`
//	}

type MoUser struct {
	Uid    string `json:"uid"`
	Name   string `json:"name"`
	Avatar string `json:"avatar"`
}

type MoConnect struct {
	Ec          int    `json:"ec"`
	Em          string `json:"em"`
	Millisecond int64  `json:"millisecond"`
	Data        struct {
		Token string `json:"token"`
		User  MoUser `json:"user"`
	} `json:"data"`
}

type MoDisconnect struct {
	Ec          int    `json:"ec"`
	Em          string `json:"em"`
	Millisecond int64  `json:"millisecond"`
	//Data        struct {
	//} `json:"data"`
}

type MoPullPayloadsItem struct {
	Type      string `json:"type"`
	Ts        int64  `json:"ts"`
	TagId     int64  `json:"tag_id"` //标识数据
	GiftName  string `json:"gift_name,omitempty"`
	GiftCount int    `json:"gift_count,omitempty"`
	User      MoUser `json:"user"`
	Content   string `json:"content,omitempty"`

	//自己新增的字段，不是人家返回的
	GameRoomId  string `json:"game_room_id"`  //游戏直播间id
	RoundGameId string `json:"round_game_id"` //局id
	HostNo      string `json:"host_no"`       //陌陌直播编号
	HostAvatar  string `json:"host_avatar"`   //主播头像
	HostName    string `json:"host_name"`     //主播名字
	HostUid     string `json:"host_uid"`      //主播id
	DataSource  uint   `json:"data_source"`   //数据来源，默认0，陌陌数据
	OnCamp      int    `bson:"on_camp"`       //所属阵营
}

type MoPull struct {
	Ec          int    `json:"ec"`
	Em          string `json:"em"`
	Millisecond int64  `json:"millisecond"`
	Data        struct {
		Payloads []MoPullPayloadsItem `json:"payloads"`
	} `json:"data"`
}

type T struct {
	Ec          int    `json:"ec"`
	Em          string `json:"em"`
	Millisecond int64  `json:"millisecond"`
	Data        struct {
		Payloads []struct {
			Type string `json:"type"`
			Ts   int64  `json:"ts"`
			User struct {
				Uid    string `json:"uid"`
				Name   string `json:"name"`
				Avatar string `json:"avatar"`
			} `json:"user"`
			GiftName  string `json:"gift_name"`
			GiftCount int    `json:"gift_count"`
		} `json:"payloads"`
	} `json:"data"`
}

//type MoPull struct {
//	Ec          int    `json:"ec"`
//	Em          string `json:"em"`
//	Millisecond int64  `json:"millisecond"`
//	Data        struct {
//		Payloads []struct {
//			Type      string `json:"type"`
//			Ts        int64  `json:"ts"`
//			GiftName  string `json:"gift_name,omitempty"`
//			GiftCount string `json:"gift_count,omitempty"`
//			User      struct {
//				Uid    string `json:"uid"`
//				Name   string `json:"name"`
//				Avatar string `json:"avatar"`
//			} `json:"user"`
//			Content string `json:"content,omitempty"`
//		} `json:"payloads"`
//	} `json:"data"`
//}

type MoUpdate struct {
	Ec          int    `json:"ec"`
	Em          string `json:"em"`
	Millisecond int64  `json:"millisecond"`
	Data        struct {
	} `json:"data"`
}

type MoStatus struct {
	Ec          int    `json:"ec"`
	Em          string `json:"em"`
	Millisecond int64  `json:"millisecond"`
	Data        struct {
		PlayId   int64         `json:"playId"`
		Status   string        `json:"status"`
		User     MoUser        `json:"user"`
		Payloads []interface{} `json:"payloads"`
	} `json:"data"`
}

type MoSendBili struct {
	Ec          int    `json:"ec"`
	Em          string `json:"em"`
	Millisecond int64  `json:"millisecond"`
	Data        bool   `json:"data"`
}
