package momo

// KafkaQueueBarrage 弹幕
type KafkaQueueBarrage struct {
	TagId      string               `json:"tag_id"`
	ActionTime int64                `json:"action_time"`
	List       []MoPullPayloadsItem `json:"list"`
}
