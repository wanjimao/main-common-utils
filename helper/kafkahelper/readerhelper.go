package kafkahelper

import (
	"context"
	"github.com/segmentio/kafka-go"
)

type ReaderConfig struct {
	Brokers []string
	Topic   string
	GroupID string
}

type ReaderHelper struct {
	r *kafka.Reader
}

func (r *ReaderHelper) ReadMessage(ctx context.Context) (kafka.Message, error) {
	return r.r.ReadMessage(ctx)
}

func NewReaderHelper(config ReaderConfig) *kafka.Reader {
	return kafka.NewReader(kafka.ReaderConfig{
		Brokers:  config.Brokers,
		GroupID:  config.GroupID,
		Topic:    config.Topic,
		MinBytes: 10e3, // 10KB
		MaxBytes: 10e6, // 10MB
	})
}

func (r *ReaderHelper) Close() error {
	return r.Close()
}
