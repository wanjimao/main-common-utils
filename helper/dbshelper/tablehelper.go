package dbshelper

import "strings"

type TableHelper interface {
	GetIntPatition(i int) int
	GetStrPatition(s string) int
}

//
type defaultTableHelper struct {
}

func NewTableHelper() defaultTableHelper {
	return defaultTableHelper{}
}

//func (h defaultTableHelper) NewTableHelper() defaultTableHelper {
//	return defaultTableHelper{}
//}

func (h defaultTableHelper) GetIntPatition(i int) int {
	return i % 100
}

func (h defaultTableHelper) GetStrPatition(s string) int {
	return h.GetPatitionHash(s) % 100
}

func (h defaultTableHelper) GetPatitionHash(s string) int {
	data := []byte(strings.Trim(s, " "))
	hash := 0
	for _, c := range data {
		hash = hash<<4 + int(c)
		x := hash & 0xF0000000
		if x != 0 {
			hash ^= x >> 24
			hash &= ^x
		}
	}
	return hash & 0x7FFFFFFF
}

//func GetIntPatition(i int) int {
//	return i % 100
//}

//func GetStrPatition(s string) int {
//	return getPatitionHash(s) % 100
//}
//
//func getPatitionHash(s string) int {
//	data := []byte(strings.Trim(s, " "))
//	hash := 0
//	for _, c := range data {
//		hash = hash<<4 + int(c)
//		x := hash & 0xF0000000
//		if x != 0 {
//			hash ^= x >> 24
//			hash &= ^x
//		}
//	}
//	return hash & 0x7FFFFFFF
//}
