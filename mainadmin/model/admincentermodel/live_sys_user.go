package admincentermodel

import (
	"gorm.io/gorm"
	"time"
)

// 后台用户表
type LiveSysUser struct {
	Id         int64          `gorm:"primaryKey;autoIncrement;comment:id" json:"后台用户表id"`
	RealName   string         `json:"real_name" gorm:"column:real_name;type:varchar(256);not null;default:'';comment:用户真实姓名"`     // 用户真实姓名
	RemarkName string         `json:"remark_name" gorm:"column:remark_name;type:varchar(256);not null;default:'';comment:备注姓名"`   // 备注姓名
	Phone      string         `json:"phone" gorm:"column:phone;type:varchar(20);not null;default:'';comment:备注姓名"`                // 用户手机号
	IsSystem   int            `json:"is_system" gorm:"column:is_system;type:tinyint;default:0;not null;comment:0:普通用户 1:系统管理"`    // 0:普通用户 1:系统管理
	LastIp     string         `json:"last_ip" gorm:"column:last_ip;type:varchar(20);not null;default:'';comment:备注姓名"`            // 最后一次登录ip
	LastTime   time.Time      `json:"last_time" gorm:"column:last_time;type:datetime"`                                            // 最后一次登录时间
	Sex        int            `json:"sex" gorm:"column:sex;type:tinyint;default:0;not null;comment:0:未知 1:男;2女;"`                 // 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
	Status     int            `json:"status" gorm:"column:status;type:tinyint;default:0;not null;comment:0为正常，1为禁止（比如离职了之类）"`     // 0为正常，1为禁止（比如离职了之类）
	Position   string         `json:"position" gorm:"column:position;type:varchar(256);not null;default:'';comment:备注姓名"`         // 用户职位，比如某某负责人
	IsReset    int            `json:"is_reset" gorm:"column:is_reset;type:tinyint;default:0;not null;comment:是否重置密码； 0:未；1：已经重置"` // 是否重置密码； 0:未；1：已经重置
	Password   string         `json:"password" gorm:"column:password;type:varchar(500);not null;default:'';comment:密码"`           // 密码
	CreatedAt  time.Time      `json:"created_at" gorm:"column:created_at;type:datetime"`
	UpdatedAt  time.Time      `json:"updated_at" gorm:"column:updated_at;type:datetime"`
	DeletedAt  gorm.DeletedAt `gorm:"index;type:datetime" json:"-"`
}
