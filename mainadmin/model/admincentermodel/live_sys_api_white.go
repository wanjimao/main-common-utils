package admincentermodel

import (
	"gorm.io/gorm"
	"time"
)

// 系统接口表
type LiveSysApiWhite struct {
	Id         int64          `gorm:"primaryKey;autoIncrement;comment:id" json:"白名单表，接口过来直接转发，不需要再去判断"`
	Url        string         `json:"url" gorm:"column:url;type:varchar(256);not null;default:'';comment: 路径"`
	PlatformId int64          `json:"platform_id" gorm:"column:platform_id;type:bigint;default:0;not null;comment:这行地方可以不写数据"` // 平台id
	CreatedAt  time.Time      `json:"created_at" gorm:"column:created_at;type:datetime"`
	UpdatedAt  time.Time      `json:"updated_at" gorm:"column:updated_at;type:datetime"`
	DeletedAt  gorm.DeletedAt `gorm:"index;type:datetime" json:"-"`
}
