package admincentermodel

import (
	"gorm.io/gorm"
	"time"
)

// 系统接口表
type LiveSysAutoPlatform struct {
	Id         int64          `gorm:"primaryKey;autoIncrement;comment:id" json:"自动为路径header添加平台id;比如回调接口不能奢望第三方给你传平台id"`
	Url        string         `json:"url" gorm:"column:url;type:varchar(256);not null;default:'';comment: 路径"`
	PlatformId int64          `json:"platform_id" gorm:"column:platform_id;type:bigint;default:0;not null;comment:平台id"` // 平台id
	CreatedAt  time.Time      `json:"created_at" gorm:"column:created_at;type:datetime"`
	UpdatedAt  time.Time      `json:"updated_at" gorm:"column:updated_at;type:datetime"`
	DeletedAt  gorm.DeletedAt `gorm:"index;type:datetime" json:"-"`
}
