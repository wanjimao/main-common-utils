package admincentermodel

import (
	"gorm.io/gorm"
	"time"
)

// 后台平台菜单表
type LiveSysMenu struct {
	Id            int64          `gorm:"primaryKey;autoIncrement;comment:id" json:"id"`
	PlatformId    int64          `json:"platform_id" gorm:"column:platform_id;type:bigint;default:0;not null;comment:平台id"`                    // 平台id
	ComponentType int            `json:"component_type" gorm:"column:component_type;type:tinyint;default:0;not null;comment:是否启用: 0 未启用 1 启用"` // 组件类型 0 开发的页面 1 iframe页面 3 外链
	ComponentUrl  string         `json:"component_url" gorm:"column:component_url;type:varchar(256);not null;default:'';comment:组建地址"`         // 组建地址
	Hidden        int            `json:"hidden" gorm:"column:hidden;type:tinyint;default:0;not null;comment:是否隐藏 0 显示  1 隐藏"`                  // 是否隐藏 0 显示  1 隐藏
	Icon          string         `json:"icon" gorm:"column:icon;type:varchar(256);not null;default:'';comment:对应图标"`                           // 对应图标
	IsCache       int            `json:"is_cache" gorm:"column:is_cache;type:tinyint;default:0;not null;comment:是否缓存 0否 1是"`                   // 是否缓存 0否 1是
	Logo          string         `json:"logo" gorm:"column:logo;type:varchar(256);not null;default:'';comment:标识"`                             // 标识
	Name          string         `json:"name" gorm:"column:name;type:varchar(256);not null;default:'';comment:名称"`                             // 名称
	Pid           int64          `json:"pid" gorm:"column:pid;type:bigint;default:0;not null;comment:父级id"`                                    // 父级id
	RedirectUrl   string         `json:"redirect_url" gorm:"column:redirect_url;type:varchar(256);not null;default:'';comment:重定向地址"`          // 重定向地址
	Sort          int64          `json:"sort" gorm:"column:sort;type:bigint;default:0;not null;comment:排序值"`                                   // 排序值
	Url           string         `json:"url" gorm:"column:url;type:varchar(256);not null;default:'';comment:token 路径"`                         // 路径
	CreateId      int64          `json:"create_id" gorm:"column:create_id;type:bigint;default:0;not null;comment:创建人id"`                       // 创建人id
	UpdateId      int64          `json:"update_id" gorm:"column:update_id;type:bigint;default:0;not null;comment:更新人id"`                       // 更新人id
	CreatedAt     time.Time      `json:"created_at" gorm:"column:created_at;type:datetime"`
	UpdatedAt     time.Time      `json:"updated_at" gorm:"column:updated_at;type:datetime"`
	DeletedAt     gorm.DeletedAt `gorm:"index;type:datetime" json:"-"`
}
