package admincentermodel

import (
	"gorm.io/gorm"
	"time"
)

// 后台系统平台表;比如,卡有圈管理后台，娃友圈管理后台，小小商城管理后台等等
type LiveSysPlatform struct {
	Id           int64          `gorm:"primaryKey;autoIncrement;comment:id" json:"id"`
	PlatformName string         `json:"platform_name" gorm:"column:platform_name;type:varchar(256);not null;default:'';comment:平台名称。比如,卡有圈管理后台，娃友圈管理后台，小小商城管理后台等等"` // 平台名称。比如,卡有圈管理后台，娃友圈管理后台，小小商城管理后台等等
	Type         int            `json:"type" gorm:"column:type;type:tinyint;default:0;not null;comment:平台类型：1:小程序，0：管理后台;"`                                         // 平台类型：1:小程序，0：管理后台
	Status       int            `json:"status" gorm:"column:status;type:tinyint;default:0;not null;comment:是否启用: 0 未启用 1 启用;"`                                      // 是否启用: 0 未启用 1 启用
	ExpireTime   int64          `json:"expire_time" gorm:"column:expire_time"`                                                                                      // 过期时间
	Url          string         `json:"url" gorm:"column:url;type:varchar(256);not null;default:'';comment: 路径"`
	Secret       string         `json:"secret" gorm:"column:secret;type:varchar(256);not null;default:'';comment:token 解析密钥"` // token 解析密钥
	CreateId     int64          `json:"create_id" gorm:"column:create_id"`                                                    // 创建人id
	CreatedAt    time.Time      `json:"created_at" gorm:"column:created_at;type:datetime"`
	UpdatedAt    time.Time      `json:"updated_at" gorm:"column:updated_at;type:datetime"`
	DeletedAt    gorm.DeletedAt `gorm:"index;type:datetime" json:"-"`
}
