package admincentermodel

import (
	"gorm.io/gorm"
	"time"
)

// 平台角色表，因为角色是基于平台的
type LiveSysRole struct {
	Id         int64          `gorm:"primaryKey;autoIncrement;comment:id" json:"id"`
	RoleName   string         `json:"role_name" gorm:"column:role_name;type:varchar(20);not null;default:'';comment:角色名称"` // 角色名称
	PlatformId int64          `json:"platform_id" gorm:"column:platform_id;type:bigint;default:0;not null;comment:平台id"`   // 平台id
	Status     int            `json:"status" gorm:"column:status;type:tinyint;default:0;not null;comment:0:启用 1:未启用;"`     // 0 启用 1 未启用
	CreatedAt  time.Time      `json:"created_at" gorm:"column:created_at;type:datetime"`
	UpdatedAt  time.Time      `json:"updated_at" gorm:"column:updated_at;type:datetime"`
	DeletedAt  gorm.DeletedAt `gorm:"index;type:datetime" json:"-"`
}
