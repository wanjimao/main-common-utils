package admincentermodel

import (
	"gorm.io/gorm"
	"time"
)

// 系统接口表
type LiveSysApi struct {
	Id         int64          `gorm:"primaryKey;autoIncrement;comment:id" json:"api的id"`
	Url        string         `json:"url" gorm:"column:url;type:varchar(256);not null;default:'';comment: 路径"`
	PlatformId int64          `json:"platform_id" gorm:"column:platform_id;type:bigint;default:0;not null;comment:平台id"`                                       // 平台id
	TypeEnum   int            `json:"type_enum" gorm:"column:type_enum;type:tinyint;default:0;not null;comment:1:POST;2:GET;3:PUT;4:DELETE;"`                  // 1:POST;2:GET;3:PUT;4:DELETE
	Status     int            `json:"status" gorm:"column:status;type:tinyint;default:0;not null;comment:是否禁用接口;0禁用；1启用;"`                                     //
	Remark     string         `json:"remark" gorm:"column:remark;type:varchar(256);not null;default:'';comment: 备注"`                                           // 是否禁用接口;0禁用；1启用
	Protocol   int            `json:"protocol" gorm:"column:protocol;type:tinyint;default:0;not null;comment: '请求前缀，0:https://和1:http://"`                     // 请求前缀，https://
	Host       string         `json:"host" gorm:"column:host;type:varchar(256);not null;default:'';comment: 域名，例如  baidu.com"`                                 // 域名，例如  baidu.com
	Port       string         `json:"port" gorm:"column:port;type:varchar(50);not null;default:'';comment: 指定端口号，默认不需要指定"`                                     // 指定端口号，默认不需要指定
	CreateId   int64          `json:"create_id" gorm:"column:create_id;type:bigint;default:0;not null;comment:创建人id""`                                         // 创建人id
	UpdateId   int64          `json:"update_id" gorm:"column:update_id;type:bigint;default:0;not null;comment:更新人id""`                                         // 更新人id
	IsAuth     int            `json:"is_auth" gorm:"column:is_auth;type:tinyint;default:0;not null;comment:是否需要鉴权，0，验证token也验证权限;1:不验证token;2:验证token 不验证权限;"` // 是否需要鉴权，0，不校验token;1:验证token也验证权限;2:验证token 不验证权限；
	CreatedAt  time.Time      `json:"created_at" gorm:"column:created_at;type:datetime"`
	UpdatedAt  time.Time      `json:"updated_at" gorm:"column:updated_at;type:datetime"`
	DeletedAt  gorm.DeletedAt `gorm:"index;type:datetime" json:"-"`
}
