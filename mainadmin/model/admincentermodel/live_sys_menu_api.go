package admincentermodel

import (
	"gorm.io/gorm"
	"time"
)

// 后台菜单接口表
type LiveSysMenuApi struct {
	Id        int64          `gorm:"primaryKey;autoIncrement;comment:id" json:"id"`
	MenuId    int64          `json:"menu_id" gorm:"column:menu_id;type:bigint;default:0;not null;comment:菜单id"`      // 菜单id
	ApiId     int64          `json:"api_id" gorm:"column:api_id;type:bigint;default:0;not null;comment:接口id"`        // 接口id
	CreateId  int64          `json:"create_id" gorm:"column:create_id;type:bigint;default:0;not null;comment:创建人id"` // 创建人id
	UpdateId  int64          `json:"update_id" gorm:"column:update_id;type:bigint;default:0;not null;comment:更新人id"` // 更新人id
	CreatedAt time.Time      `json:"created_at" gorm:"column:created_at;type:datetime"`
	UpdatedAt time.Time      `json:"updated_at" gorm:"column:updated_at;type:datetime"`
	DeletedAt gorm.DeletedAt `gorm:"index;type:datetime" json:"-"`
}
