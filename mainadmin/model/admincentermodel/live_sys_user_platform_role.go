package admincentermodel

import (
	"gorm.io/gorm"
	"time"
)

// 用户平台表，因为一个用户可以有多个平台权限，多是多对多
type LiveSysUserPlatformRole struct {
	Id         int64          `gorm:"primaryKey;autoIncrement;comment:id" json:"用户平台角色表id"`
	UserId     int64          `json:"user_id" gorm:"column:user_id;type:bigint;default:0;not null;comment:'用户id'"`         // 用户id
	PlatformId int64          `json:"platform_id" gorm:"column:platform_id;type:bigint;default:0;not null;comment:'平台id'"` // 平台id
	RoleId     int64          `json:"role_id" gorm:"column:role_id;type:bigint;default:0;not null;comment:角色id"`           // 角色id
	CreateId   int64          `json:"create_id" gorm:"column:create_id;type:bigint;default:0;not null;comment:'创建人id'"`    // 创建人id
	CreatedAt  time.Time      `json:"created_at" gorm:"column:created_at;type:datetime"`
	UpdatedAt  time.Time      `json:"updated_at" gorm:"column:updated_at;type:datetime"`
	DeletedAt  gorm.DeletedAt `gorm:"index;type:datetime" json:"-"`
}
