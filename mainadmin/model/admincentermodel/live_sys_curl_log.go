package admincentermodel

import (
	"gorm.io/gorm"
	"time"
)

// FdWechatCurlLog
//
//	网关信请求记录
type LiveSysCurlLog struct {
	Id           int64          `gorm:"primaryKey;autoIncrement;comment:id" json:"id"`
	UserId       int64          `gorm:"column:user_id;type:bigint;not null;default:0;comment:'用户ID'" json:"userId"`
	Url          string         `gorm:"column:url;index;type:varchar(100);not null;default:'';comment:'请求体'" json:"url"`
	LogType      string         `gorm:"column:log_type;type:varchar(100);not null;default:'';comment:'日志类型'" json:"logType"`
	Method       string         `gorm:"column:method;type:varchar(100);not null;default:'';comment:'请求体'" json:"method"`
	RequestInfo  string         `gorm:"column:request_info;type:text;not null;comment:'请求体'" json:"requestInfo"`
	ResponseInfo string         `gorm:"column:response_info;type:text;not null;comment:'返回体'" json:"responseInfo"`
	CreatedAt    time.Time      `json:"created_at" gorm:"column:created_at;type:datetime"`
	UpdatedAt    time.Time      `json:"updated_at" gorm:"column:updated_at;type:datetime"`
	DeletedAt    gorm.DeletedAt `gorm:"index;type:datetime" json:"-"`
}
