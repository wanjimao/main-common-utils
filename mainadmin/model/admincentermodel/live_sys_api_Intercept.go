package admincentermodel

import (
	"gorm.io/gorm"
	"time"
)

// 系统接口表
type LiveSysApiIntercept struct {
	Id        int64          `gorm:"primaryKey;autoIncrement;comment:id" json:"自动拦截接口，如果访问的路径，是里面的路径，就不继续转发，直接返回成功"`
	Url       string         `json:"url" gorm:"column:url;type:varchar(256);not null;default:'';comment: 路径"`
	CreatedAt time.Time      `json:"created_at" gorm:"column:created_at;type:datetime"`
	UpdatedAt time.Time      `json:"updated_at" gorm:"column:updated_at;type:datetime"`
	DeletedAt gorm.DeletedAt `gorm:"index;type:datetime" json:"-"`
}
