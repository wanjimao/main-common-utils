package admincentermodel

import (
	"gorm.io/gorm"
	"time"
)

// 角色菜单表
type LiveSysRoleManu struct {
	Id        int64          `gorm:"primaryKey;autoIncrement;comment:id" json:"id"`
	RoleId    int64          `json:"role_id" gorm:"column:role_id;type:bigint;default:0;not null;comment:角色id"`      // 角色id
	MenuId    int64          `json:"menu_id" gorm:"column:menu_id;type:bigint;default:0;not null;comment:菜单id"`      // 菜单id
	CreateId  int64          `json:"create_id" gorm:"column:create_id;type:bigint;default:0;not null;comment:创建人id"` // 创建人id
	UpdateId  int64          `json:"update_id" gorm:"column:update_id;type:bigint;default:0;not null;comment:更新人id"` // 更新人id
	CreatedAt time.Time      `json:"created_at" gorm:"column:created_at;type:datetime"`
	UpdatedAt time.Time      `json:"updated_at" gorm:"column:updated_at;type:datetime"`
	DeletedAt gorm.DeletedAt `gorm:"index;type:datetime" json:"-"`
}
