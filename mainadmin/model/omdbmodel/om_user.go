package omdbmodel

import "time"

// 用户表
type OmUser struct {
	Uid        int64     `json:"uid" gorm:"column:uid"`                 // 用户id
	Phone      string    `json:"phone" gorm:"column:phone"`             // 手机号
	RegType    int       `json:"reg_type" gorm:"column:reg_type"`       // 注册方式 1-一键登录 2-短信 2-微信授权 3-微信小程序 4-苹果授权
	OpenUDID   string    `json:"openUDID" gorm:"column:openUDID"`       // 设备号
	CUDID      string    `json:"cUDID" gorm:"column:cUDID"`             // 真实设备号
	Ip         int64     `json:"ip" gorm:"column:ip"`                   // IP地址
	AppVersion string    `json:"app_version" gorm:"column:app_version"` // 版本号
	Model      string    `json:"model" gorm:"column:model"`             // 手机型号
	AppChannel string    `json:"app_channel" gorm:"column:app_channel"` // 应用渠道
	Os         string    `json:"os" gorm:"column:os"`                   // 操作系统
	CityCode   string    `json:"city_code" gorm:"column:city_code"`     // 城市编码
	Lat        string    `json:"lat" gorm:"column:lat"`                 // 纬度
	Lng        string    `json:"lng" gorm:"column:lng"`                 // 经度
	Status     int       `json:"status" gorm:"column:status"`           // 状态 0 正常, 1 注销
	CreatedAt  time.Time `json:"created_at" gorm:"column:created_at"`   // 创建时间
	UpdatedAt  time.Time `json:"updated_at" gorm:"column:updated_at"`   // 更新时间
}
