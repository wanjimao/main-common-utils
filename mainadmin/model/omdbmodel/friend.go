package omdbmodel

import "time"

// 好友表(好友申请表)
type Friend struct {
	Id        int64     `json:"id" gorm:"column:id"`                 // 记录 id
	Tid       int64     `json:"tid" gorm:"column:tid"`               // talk id
	Ftid      int64     `json:"ftid" gorm:"column:ftid"`             // 关系方的 talk id
	Status    int       `json:"status" gorm:"column:status"`         // 关系状态: -1 无状态;0 好友申请;1 是好友;2 黑名单;3 被拒绝;4 删除;5 请求过期
	Bitmap    int64     `json:"bitmap" gorm:"column:bitmap"`         // 方便扩展的 bitmap 位, cache 的落地存储, 冗余 status 字段
	Intimacy  int       `json:"intimacy" gorm:"column:intimacy"`     // 亲密度
	Interest  int       `json:"interest" gorm:"column:interest"`     // 兴趣度
	Initiator int       `json:"initiator" gorm:"column:initiator"`   // 请求发起者
	RequestAt time.Time `json:"request_at" gorm:"column:request_at"` // 请求时间
	CreatedAt time.Time `json:"created_at" gorm:"column:created_at"` // 创建记录时间
	UpdatedAt time.Time `json:"updated_at" gorm:"column:updated_at"` // 记录更新时间
	IsDeleted int       `json:"is_deleted" gorm:"column:is_deleted"` // 是否被删除;(1:未被删除;2已经被删除)
}
