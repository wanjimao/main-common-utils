package mongoservice

import (
	"gitee.com/wanjimao/main-common-utils/enums"
)

type defaultMongoService struct {
	EnvServer string
}

func NewMongoService(envServer string) defaultMongoService {
	return defaultMongoService{
		EnvServer: envServer,
	}
}

type MongoService interface {
}

// GetPetBarrageCollection todo 获取弹幕表
func (s defaultMongoService) GetPetBarrageCollection() string {
	//return "/" + s.EnvServer + "/" + enums.CollectPetBarrage
	return s.EnvServer + "_" + enums.CollectPetBarrage
}

// GetPetConsumeErrorCollection todo 获取报错日志表
func (s defaultMongoService) GetPetConsumeErrorCollection() string {
	//return "/" + s.EnvServer + "/" + enums.CollectPetConsumeError
	return s.EnvServer + "_" + enums.CollectPetConsumeError
}

// GetRoundGameEndUserDetailCollection todo 局详情表
func (s defaultMongoService) GetRoundGameEndUserDetailCollection() string {
	//return "/" + s.EnvServer + "/" + enums.CollectRoundGameEndUserDetail
	return s.EnvServer + "_" + enums.CollectRoundGameEndUserDetail
}

func (s defaultMongoService) GetGameRoom() string {
	//return "/" + s.EnvServer + "/" + enums.CollectRoundGameEndUserDetail
	return s.EnvServer + "_" + enums.CollectGameRoom
}

func (s defaultMongoService) GetRoundGame() string {
	//return "/" + s.EnvServer + "/" + enums.CollectRoundGameEndUserDetail
	return s.EnvServer + "_" + enums.CollectRoundGame
}

// GetJobCommonErrorCollection 获取错误信息表
func (s defaultMongoService) GetJobCommonErrorCollection() string {
	//return "/" + s.svcCtx.Config.EnvServer + "/" + enums.CollectJobCommonErrorAdd
	return s.EnvServer + "_" + enums.CollectJobCommonErrorAdd
}

// GetPetConfigGiftCollection 获取礼物配置表
func (s defaultMongoService) GetPetConfigGiftCollection() string {
	//return "/" + s.svcCtx.Config.EnvServer + "/" + enums.CollectJobCommonErrorAdd
	return s.EnvServer + "_" + enums.CollectPetConfigGift
}

// GetPetMomoCurlCollection 获取所有的curl请求表
func (s defaultMongoService) GetPetMomoCurlCollection() string {
	//return "/" + s.svcCtx.Config.EnvServer + "/" + enums.CollectJobCommonErrorAdd
	return s.EnvServer + "_" + enums.CollectPetMomoCurl
}

// GetPetMomoBarrageTransitCollection 获取弹幕中转表
func (s defaultMongoService) GetPetMomoBarrageTransitCollection() string {
	//return "/" + s.svcCtx.Config.EnvServer + "/" + enums.CollectJobCommonErrorAdd
	return s.EnvServer + "_" + enums.CollectPetMomoBarrageTransit
}
