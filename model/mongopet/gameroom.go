package mongopet

// GameRoom 登录的时候会创建  游戏房间表
type GameRoom struct {
	ID           string `bson:"id"`             //游戏直播间id，主播点击一次login 就会创建一条记录
	HostNo       string `bson:"host_no"`        //绑定的第三方直播间编号
	LoginType    int8   `bson:"login_type"`     //登录类型;0:默认的，没有指定，业务中暂时按照访客模式处理;1:访客模式;2:密钥登录
	GameRoomType int8   `bson:"game_room_type"` //直播间类型;0:默认的陌陌直播间;1:抖音;2:快手
	Token        string `bson:"token"`          //直播间token
	HostUid      string `bson:"host_uid"`       //主播游戏内用户id UID跟MOMOID/host_no一对一关系
	HostName     string `bson:"host_name"`      //主播游戏内用户name
	HostAvatar   string `bson:"host_avatar"`    //主播游戏内用户name
	CreateAt     string `bson:"create_at"`      //创建时间
	UpdateAt     string `bson:"update_at"`      //更新时间时间
	EndAt        string `bson:"end_at"`         //更新时间时间
}
