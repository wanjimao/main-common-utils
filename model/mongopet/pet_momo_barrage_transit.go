package mongopet

import "time"

type PetMomoBarrageTransit struct {
	_Id        string    `bson:"_id"`         // id
	TagId      string    `bson:"tag_id"`      // id也就是id
	List       string    `bson:"list"`        // 昵称
	HostNo     string    `json:"host_no"`     //陌陌直播编号
	HostAvatar string    `json:"host_avatar"` //主播头像
	HostName   string    `json:"host_name"`   //主播名字
	HostUid    string    `json:"host_uid"`    //主播id
	ActionTime int64     `bson:"action_time"` // 昵称
	CreatedAt  time.Time `bson:"created_at"`  // 创建时间
	UpdatedAt  time.Time `bson:"updated_at"`  // 更新时间
}
