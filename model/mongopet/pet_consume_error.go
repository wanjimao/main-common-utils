package mongopet

import "time"

// todo  弹幕数据库

type PetConsumeError struct {
	_Id       int64     `bson:"_id"`        // id
	Id        string    `bson:"id"`         // id
	Topic     string    `bson:"topic"`      // 昵称
	TagId     string    `bson:"tag_id"`     //消息的唯一id
	Group     string    `bson:"group"`      // 组
	ErrorMsg  string    `bson:"error_msg"`  // 报错原因
	CreatedAt time.Time `bson:"created_at"` // 创建时间
	UpdatedAt time.Time `bson:"updated_at"` // 更新时间
	KafkaData string    `bson:"kafka_data"`
}
