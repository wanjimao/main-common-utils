package mongopet

type RoundGameEndUserDetail struct {
	_Id                int64  `bson:"_id"`                                              // id
	GameRoomId         string `json:"game_room_id" bson:"game_room_id"`                 //当前局id
	HostNo             string `json:"host_no" bson:"host_no"`                           //主播信息
	RoundGameId        string `json:"round_game_id" bson:"round_game_id"`               //当前局
	RoundStartAt       int64  `json:"round_start_at" bson:"round_start_at"`             //当前局开始时间
	RoundCreateAt      int64  `json:"round_create_at" bson:"round_create_at"`           //当前局战斗时间
	RoundEndAt         int64  `json:"round_end_at" bson:"round_end_at"`                 //当前局结束时间
	CurrentAllPoint    int64  `json:"current_all_point" bson:"current_all_point"`       //当前总积分
	WhoWin             int32  `json:"who_win" bson:"who_win"`                           //胜利方
	Uid                string `json:"uid" bson:"uid"`                                   //玩家id
	Name               string `json:"name" bson:"name"`                                 //玩家id
	Avatar             string `json:"avatar" bson:"avatar"`                             //用户头像
	OnCamp             int32  `json:"on_camp" bson:"on_camp"`                           //所属阵营;1:左方阵营;2:右方阵营
	CreatePoint        int64  `json:"create_point" bson:"create_point"`                 //创建兵积分
	KillPoint          int64  `json:"kill_point" bson:"kill_point"`                     //击杀兵积分
	BossPoint          int64  `json:"boss_point" bson:"boss_point"`                     //消耗boss积分
	TotalOriginalPoint int64  `json:"total_original_point" bson:"total_original_point"` //基础总积分原始积分
	PointBank          int    `json:"point_bank" bson:"point_bank"`                     //瓜分前本局总积分排名
	FinalPointBank     int    `json:"finally_point_bank" bson:"finally_point_bank"`     //最终本局总积分排名
	CampPointBank      int    `json:"camp_point_bank" bson:"camp_point_bank"`           //本局阵营内的积分排名
	DivideProportion   string `json:"divide_proportion" bson:"divide_proportion"`       //瓜分比例
	DividePoint        int64  `json:"divide_point" bson:"divide_point"`                 //瓜分获得的积分
	FinallyPoint       int64  `json:"finally_proportion" bson:"finally_proportion"`     //最终积分
	SourceType         int    `json:"source_type" bson:"source_type"`                   //数据来源类型，平台类型；0:momo
	OnlyId             string `json:"only_id" bson:"only_id"`                           //唯一id
	TagId              string `json:"tag_id" bson:"tag_id"`                             //写入kafka的批次id,一次写入为1批
	WinningTotal       int64  `json:"winning_total" bson:"winning_total"`               //当前累计胜场
	TotalPoint         int64  `json:"total_point" bson:"total_point"`                   //当前累计总积分
	ActionTime         int64  `json:"action_time" bson:"action_time"`                   //动作时间
}
