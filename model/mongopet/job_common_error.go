package mongopet

import "time"

// todo  弹幕数据库

type JobCommonError struct {
	_Id       string    `bson:"_id"`        // id
	Id        string    `bson:"id"`         // id
	ErrorMsg  string    `bson:"error_msg"`  // 报错原因
	Data      string    `bson:"data"`       // 报错原因
	JobType   string    `bson:"job_type"`   // 1:SyncBarrageToClickHouseLogic:消费弹幕任务;2:
	IsRead    int8      `bson:"is_read"`    // 是否已读;0:未读;1:已读
	CreatedAt time.Time `bson:"created_at"` // 创建时间
	UpdatedAt time.Time `bson:"updated_at"` // 更新时间
}
