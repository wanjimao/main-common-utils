package mongopet

type RoundGame struct {
	ID              string `bson:"id"`                //游戏直播间id，主播点击一次login 就会创建一条记录
	GameRoomId      string `bson:"game_room_id"`      //游戏直播间的id
	GameRoomType    int8   `bson:"game_room_type"`    //直播间类型;0:默认的陌陌直播间;1:抖音;2:快手
	CreateAt        int64  `bson:"create_at"`         //创建时间
	CurrentAllPoint int64  `bson:"current_all_point"` //当前积分池总积分
	WhoWin          int8   `bson:"who_win"`           //胜利的人是谁，1:红方;2:蓝方;3:平局
	StartAt         int64  `bson:"start_at"`          //开始战斗时间
	EndAt           int64  `bson:"end_at"`            //战斗结束时间
	UpdateAt        string `bson:"update_at"`         //更新时间时间
}
