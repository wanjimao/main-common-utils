package mongopet

import "time"

// todo  弹幕数据库
type PetConfigGift struct {
	_Id       string    `bson:"_id"` // id
	Id        string    `bson:"id"`  //游戏直播间id，主播点击一次login 就会创建一条记录
	GiftName  string    `bson:"gift_name"`
	Price     int64     `bson:"price"`
	Status    int       `bson:"status"`     //1为禁用，0为正常
	OnlyId    string    `bson:"only_id"`    //记录的唯一id
	CreatedAt time.Time `bson:"created_at"` // 创建时间
	UpdatedAt time.Time `bson:"updated_at"` // 更新时间
	DeleteAt  int64     `bson:"delete_at"`  //删除时间
	UpdateId  int64     `bson:"update_id"`  //更新人
	CreateId  int64     `bson:"create_id"`  //创建人
}
