package mongopet

// todo  弹幕数据库
type PetBarrage struct {
	_Id         string `bson:"_id"` // id
	Type        string `bson:"type"`
	HostNo      string `bson:"host_no"`       //主播直播码
	GameRoomId  string `bson:"game_room_id"`  //房间id
	RoundGameId string `bson:"round_game_id"` //局id
	Ts          int64  `bson:"ts"`
	GiftName    string `bson:"gift_name"`
	GiftCount   int    `bson:"gift_count"`
	Content     string `bson:"content"`
	Uid         string `bson:"uid"`
	Name        string `bson:"name"`
	Avatar      string `bson:"avatar"`
	ActionTime  int64  `bson:"action_time"`
	TagId       string `bson:"tag_id"`      //批次id
	OnlyId      string `bson:"only_id"`     //记录的唯一id
	OnCamp      int    `bson:"on_camp"`     //所属阵营;1:左方阵营;2:右方阵营
	HostAvatar  string `bson:"host_avatar"` //主播头像
	HostName    string `bson:"host_name"`   //主播名字
	HostUid     string `bson:"host_uid"`    //主播id
	UnitPrice   int64  `bson:"unit_price"`  //单价
	TotalPrice  int64  `bson:"total_price"` //总价
}
