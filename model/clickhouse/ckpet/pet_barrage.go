package ckpet

import (
	"time"
)

// todo  弹幕数据库
type PetBarrage struct {
	Id          uint64    `json:"id" gorm:"column:id;comment:id"`
	TagId       string    `json:"tagId" gorm:"column:tag_id;type:varchar(200);default:'';comment:批次id"`                            // 唯一id
	OnlyId      string    `json:"onlyId" gorm:"column:only_id;type:varchar(200);default:'';comment:唯一id"`                          // 唯一id
	ActionTime  int64     `json:"actionTime" gorm:"column:action_time;type:bigint;default:0;comment:行为时间"`                       // 唯一id
	Avatar      string    `json:"avatar" gorm:"column:avatar;type:varchar(500);not null;default:'';comment: 头像"`                   // 标题、名称
	Name        string    `json:"name" gorm:"column:name;type:varchar(200);not null;default:'';comment: 姓名"`                       // 标题、名称
	Uid         string    `json:"uid" gorm:"column:uid;type:varchar(200);not null;default:'';comment: 用户id"`                       // 标题、名称
	Content     string    `json:"content" gorm:"column:content;type:varchar(200);not null;default:'';comment: 弹幕内容"`             // 标题、名称
	GiftCount   int64     `json:"giftCount" gorm:"column:gift_count;type:bigint;default:0;not null;comment:礼物数量;"`               // 跳转类型 0:小程序内部跳转;1:H5;2:公众号;
	GiftName    string    `json:"giftName" gorm:"column:gift_name;type:varchar(200);not null;default:'';comment: 礼物名字"`          // 标题、名称
	RoundGameId string    `json:"roundGameId" gorm:"column:round_game_id;type:varchar(100);not null;default:'';comment: 局id"`       // 标题、名称
	GameRoomId  string    `json:"gameRoomId" gorm:"column:game_room_id;type:varchar(100);not null;default:'';comment: 游戏直播间id"` // 标题、名称
	HostNo      string    `json:"hostNo" gorm:"column:host_no;type:varchar(100);not null;default:'';comment: 主播编号"`              // 标题、名称
	HostAvatar  string    `json:"hostAvatar" gorm:"column:host_avatar;type:varchar(100);not null;default:'';comment: 主播头像"`      // 标题、名称
	HostName    string    `json:"hostName" gorm:"column:host_name;type:varchar(100);not null;default:'';comment: 主播名字"`          // 标题、名称
	HostUid     string    `json:"hostUid" gorm:"column:host_uid;type:varchar(100);not null;default:'';comment: 主播id"`              // 标题、名称
	UnitPrice   int64     `json:"unitPrice" gorm:"column:unit_price;type:bigint;default:0;comment:单价"`                             //单价
	TotalPrice  int64     `json:"totalPrice" gorm:"column:total_price;type:bigint;default:0;comment:单价"`                           //总价
	Ts          int64     `json:"ts" gorm:"column:ts;type:bigint;default:0;comment:时间"`                                            // 唯一id
	TsString    string    `json:"tsString" gorm:"column:ts_string;type:varchar(100);not null;default:'';comment: 时间字符串"`        // 标题、名称
	DataSource  int64     `json:"dataSource" gorm:"column:data_source;type:bigint;default:0;not null;comment:0:陌陌 1:抖音;2:快手"`  // 	// 更新人id
	CreatedAt   time.Time `json:"created_at" gorm:"column:created_at;type:datetime"`
	UpdatedAt   time.Time `json:"updated_at" gorm:"column:updated_at;type:datetime"`
	//DeletedAt   gorm.DeletedAt `json:"-" gorm:"index;type:datetime"`
}

//HostAvatar  string `bson:"host_avatar"` //主播头像
//HostName    int    `bson:"host_name"`   //主播名字
//HostUid     int    `bson:"host_uid"`     //主播id
