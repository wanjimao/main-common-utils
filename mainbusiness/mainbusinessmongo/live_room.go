package mainbusinessmongo

// DanceRoom 登录的时候会创建  游戏房间表
type LiveDanceRoom struct {
	Id_               string `bson:"_id"`            // id
	Token             string `bson:"token"`          //
	AccessToken       string `bson:"access_token"`   //
	RoomId            string `bson:"room_id"`        //
	AnchorOpenId      string `bson:"anchor_open_id"` //
	AvatarUrl         string `bson:"avatar_url"`     //
	Nickname          string `bson:"nickname"`       //
	TaskIdLiveComment string `bson:"task_id_live_comment"`
	TaskIdLiveGift    string `bson:"task_id_live_gift"`
	TaskIdLiveLike    string `bson:"task_id_live_like"`
	UpdateAt          int64  `bson:"update_at"`
	CreatedAt         int64  `bson:"created_at"`
	CreatedAtString   string `bson:"create_at"`
}
