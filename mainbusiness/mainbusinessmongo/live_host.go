package mainbusinessmongo

// DanceHost 主播表
type LiveHost struct {
	Id_                   string `bson:"_id"`                       // id
	AnchorOpenId          string `bson:"anchor_open_id"`            //主播编号
	AvatarUrl             string `bson:"avatar_url"`                //主播头像
	AvatarUrlAll          string `bson:"avatar_url_all"`            //主播头像
	Nickname              string `bson:"nickname"`                  //主播昵称
	NicknameAll           string `bson:"nickname_all"`              //主播昵称
	NumberOfMatches       int    `bson:"number_of_matches"`         //比赛开启总场次，
	FirstOfMatchStartTime int64  `bson:"first_of_match_start_time"` //首场比赛开始时间
	FirstOfMatchEndTime   int64  `bson:"first_of_match_end_time"`   //首场比赛结束时间
	EndOfMatchStartTime   int64  `bson:"end_of_match_start_time"`   //最后一场比赛开启时间
	EndOfMatchEndTime     int64  `bson:"end_of_match_end_time"`     //最后一场比赛结束时间
	CumulativeGiftIncome  int64  `bson:"cumulative_gift_income"`    //累计礼物收入
	UpdateAt              int64  `bson:"update_at"`                 //更新时间
	CreatedAt             int64  `bson:"created_at"`                //创建时间
	CreatedAtString       string `bson:"create_at"`                 //创建时间
}
