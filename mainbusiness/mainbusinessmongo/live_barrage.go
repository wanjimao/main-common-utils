package mainbusinessmongo

// LiveBarrage todo  弹幕数据库
type LiveBarrage struct {
	Id_                      string `bson:"_id"`             // id
	Token                    string `bson:"token"`           //
	RoomId                   string `bson:"room_id"`         //
	BarrageType              int    `bson:"barrage_type"`    //1:评论;2:礼物;3:点赞;
	AnchorOpenId             string `bson:"anchor_open_id"`  //
	AvatarUrlHost            string `bson:"avatar_url_host"` //
	NicknameHost             string `bson:"nickname_host"`   //
	ModelId                  string `bson:"model_id"`        //
	RoundId                  string `bson:"round_id"`        //回合id
	BarrageId                string `bson:"barrage_id"`      //回合id
	UpdateAtRoom             int64  `bson:"update_at_room"`
	CreatedAtRoom            int64  `bson:"created_at_room"`
	CreatedAtRoomString      string `bson:"created_at_room_string"`
	TaskIdLiveComment        string `bson:"task_id_live_comment"`
	TaskIdLiveGift           string `bson:"task_id_live_gift"`
	TaskIdLiveLike           string `bson:"task_id_live_like"`
	CreatedAtRoomModel       int64  `bson:"created_at_room_model"`
	CreatedAtRoomModelString string `bson:"created_at_room_model_string"`
	ProblemId                int64  `bson:"problem_id"`       //问题id
	ProblemContent           string `bson:"problem_content"`  //问题内容
	ProblemAnswerA           string `bson:"problem_answer_a"` //答案A
	ProblemAnswerB           string `bson:"problem_answer_b"` //答案B
	ProblemRight             int32  `bson:"problem_right"`    //正确答案;0:A真确;1:B正确
	UpdateAtModel            int64  `bson:"update_at_model"`
	CreatedAtModel           int64  `bson:"created_at_model"`
	CreatedAtModelString     string `bson:"create_at_model_string"`

	UnitPrice  int64 `bson:"unit_price"`  //礼物单价
	TotalPrice int64 `bson:"total_price"` //礼物总价

	BatchId   string `bson:"batch_id"`
	SecOpenid string `bson:"sec_openid"`
	AvatarUrl string `bson:"avatar_url"`
	Nickname  string `bson:"nickname"`
	Timestamp int64  `bson:"timestamp"`

	MsgId string `bson:"msg_id"`

	SecGiftId       string `bson:"sec_gift_id"`
	GiftNum         int    `bson:"gift_num"`
	GiftValue       int    `bson:"gift_value"`
	GiftTest        int    `bson:"gift_test"`
	LikeNum         int    `bson:"like_num"`
	Content         string `bson:"content"`
	GiftSinglePrice int    `bson:"gift_single_price"` //礼物单价
	GiftName        string `bson:"gift_name"`         //礼物名字

}
