package mainbusinessmongo

type LiveRoundGameEndUserDetail struct {
	Id_ string `bson:"_id"` // id

	//##############################回合信息##########################################
	Token               string `bson:"token"`          //
	RoomId              string `bson:"room_id"`        //
	AnchorOpenId        string `bson:"anchor_open_id"` //
	AvatarUrl           string `bson:"avatar_url"`     //
	Nickname            string `bson:"nickname"`       //
	ModelId             string `bson:"model_id"`       //
	UpdateAtRoom        int64  `bson:"update_at_room"`
	CreatedAtRoom       int64  `bson:"created_at_room"`
	CreatedAtRoomString string `bson:"create_at_room_string"`

	CreatedAtModel       int64  `bson:"created_at_model"`
	CreatedAtModelString string `bson:"create_at_model_string"`

	TaskIdLiveComment string `bson:"task_id_live_comment"`
	TaskIdLiveGift    string `bson:"task_id_live_gift"`
	TaskIdLiveLike    string `bson:"task_id_live_like"`

	//###############################其他信息#########################################
	CurrentModelTotalScore      int    `json:"currentModelTotalScore" bson:"current_model_total_score"`           //当前总积分
	TotalScore                  int    `json:"totalScore" bson:"total_score"`                                     //当前累计总星光值
	CurrentModelTotalExperience int    `json:"currentModelTotalExperience" bson:"current_model_total_experience"` //当前总积分
	TotalExperience             int    `json:"totalExperience" bson:"total_experience"`                           //当前累计总星光值
	WhoWin                      int32  `json:"whoWin" bson:"who_win"`                                             //胜利方
	Uid                         string `json:"uid" bson:"uid"`                                                    //玩家id
	CurrentModelGiftNum         int    `json:"currentModelGiftNum" bson:"current_model_gift_num"`                 //礼物数量
	CurrentModelGiftMoney       int64  `json:"currentModelGiftMoney" bson:"current_model_gift_money"`             //礼物金额
	Name                        string `json:"name" bson:"name"`                                                  //玩家名字
	Avatar                      string `json:"avatar" bson:"avatar"`                                              //用户头像
	OnCamp                      int32  `json:"onCamp" bson:"on_camp"`                                             //所属阵营;1:左方阵营;2:右方阵营
	OnlyId                      string `json:"onlyId" bson:"only_id"`                                             //唯一id
	IntoMongoBatchId            string `json:"tag_id" bson:"into_mongo_batch_id"`                                 //写入mongo的批次id,一次写入为1批
	WinningTotal                int64  `json:"winning_total" bson:"winning_total"`                                //当前累计胜场
	CreatedAt                   int64  `bson:"created_at"`
	CreatedAtString             string `bson:"create_at"`
}
