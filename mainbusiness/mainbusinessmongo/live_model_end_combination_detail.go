package mainbusinessmongo

type LiveRoundGameEndCombinationDetail struct {
	Id_              string `bson:"_id"`                 // id
	HostNickname     string `bson:"host_nickname"`       //主播昵称
	HostNo           string `bson:"host_no"`             //主播编号
	RoomId           string `bson:"room_id"`             //直播间id
	RoomCreateAt     int64  `bson:"room_create_at"`      //直播间id
	ModelId          string `bson:"model_id"`            //局id
	ModelCreateAt    int64  `bson:"model_create_at"`     //直播间id
	ZSetKey          string `bson:"z_set_key"`           //主动at人的id
	HashDetailMapKey string `bson:"hash_detail_map_key"` //组合详情
	BeginTime        string `bson:"begin_time"`          //区间开始时间
	EndTime          string `bson:"end_time"`            //区间结束时间
	UidActive        string `bson:"uid_active"`          //主动at人的id
	UidActiveName    string `bson:"uid_active_name"`     //
	UidPassive       string `bson:"uid_passive"`         //
	UidPassiveName   string `bson:"uid_passive_name"`    //
	ActiveScore      int64  `bson:"active_score"`        //积分
	PassiveScore     int64  `bson:"passive_score"`       //积分
	TotalScore       int64  `bson:"total_score"`         //总积分
	CreatedAt        int64  `bson:"created_at"`
	CreatedAtString  string `bson:"create_at"`
}

type CombinationDetailList struct {
	ZSetKey          string   `bson:"z_set_key"`           //主动at人的id
	HashDetailMapKey []string `bson:"hash_detail_map_key"` //组合详情
}
