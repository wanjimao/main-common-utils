package mainbusinessmongo

//主播暂时怎么处理

// DanceRoom 登录的时候会创建  游戏房间表
type LiveRoomModel struct {
	Id_   string `bson:"_id"`   // id
	Token string `bson:"token"` //
	//AccessToken     string        `bson:"access_token"`   //
	RoomId              string `bson:"room_id"`        //
	AnchorOpenId        string `bson:"anchor_open_id"` //主播id
	AvatarUrl           string `bson:"avatar_url"`     //主播头像
	Nickname            string `bson:"nickname"`       //主播名字
	ModelId             string `bson:"model_id"`       //
	UpdateAtRoom        int64  `bson:"update_at_room"`
	CreatedAtRoom       int64  `bson:"created_at_room"`
	CreatedAtRoomString string `bson:"create_at_room_string"`
	TaskIdLiveComment   string `bson:"task_id_live_comment"`
	TaskIdLiveGift      string `bson:"task_id_live_gift"`
	TaskIdLiveLike      string `bson:"task_id_live_like"`

	JoinNumber     int64 `bson:"join_number"`     //参与人数
	GiftMoney      int64 `bson:"gift_money"`      //礼物收入
	RedScore       int64 `bson:"red_score"`       //红队分数(星光值)
	RedExperience  int64 `bson:"red_experience"`  //红队分数（经验值）
	BlueScore      int64 `bson:"blue_score"`      //蓝队分数(星光值)
	BlueExperience int64 `bson:"blue_experience"` //蓝队分数（经验值）
	WhoWin         int   `bson:"who_win"`         //获胜阵营

	ModelType int `bson:"model_type"` //模式类型，0是默认模式，1是双人模式

	UpdateAt         int64  `bson:"update_at"`
	CreatedAt        int64  `bson:"created_at"`   //比赛开始时间
	ModelEndAt       int64  `bson:"model_end_at"` //比赛结束时间
	ModelEndAtString string `bson:"model_end_at_string"`
	CreatedAtString  string `bson:"create_at"`
}
