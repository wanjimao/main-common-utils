package mainbusinessmongo

import (
	"time"
)

// DanceRoom 登录的时候会创建  游戏房间表
type LiveRoomProblem struct {
	Id_             string    `bson:"_id"`              // id
	RoomId          string    `bson:"room_id"`          //房间id
	ProblemId       string    `bson:"problem_id"`       //答案id
	ProblemContent  string    `bson:"problem_content"`  //文本内容
	ProblemAnswerA  string    `bson:"problem_answer_a"` //A答案
	ProblemAnswerB  string    `bson:"problem_answer_b"` //B答案
	ProblemRight    int       `bson:"problem_right"`    //1:A对；2：B对
	UpdateAt        int64     `bson:"update_at"`
	CreatedAt       int64     `bson:"created_at"`
	CreatedAtString string    `bson:"create_at"`
	ExpireAt        time.Time `bson:"expireAt"`
}
