package mainbusinessmongo

// DanceRoomModelRound 登录的时候会创建  游戏房间表
type LiveRoomModelRound struct {
	Id_          string `bson:"_id"`            // id
	Token        string `bson:"token"`          //
	RoomId       string `bson:"room_id"`        //
	AnchorOpenId string `bson:"anchor_open_id"` //
	AvatarUrl    string `bson:"avatar_url"`     //
	Nickname     string `bson:"nickname"`       //
	ModelId      string `bson:"model_id"`       //
	ModelType    int    `bson:"model_type"`     //模式类型，0是默认模式，1是双人模式

	RoundId                  string `bson:"round_id"` //回合id
	UpdateAtRoom             int64  `bson:"update_at_room"`
	CreatedAtRoom            int64  `bson:"created_at_room"`
	CreatedAtRoomString      string `bson:"create_at_room_string"`
	TaskIdLiveComment        string `bson:"task_id_live_comment"`
	TaskIdLiveGift           string `bson:"task_id_live_gift"`
	TaskIdLiveLike           string `bson:"task_id_live_like"`
	UpdateAtRoomModel        int64  `bson:"update_at_room_model"`
	CreatedAtRoomModel       int64  `bson:"created_at_room_model"`
	CreatedAtRoomModelString string `bson:"create_at_room_model_string"`
	ProblemId                int64  `bson:"problem_id"`       //问题id
	ProblemContent           string `bson:"problem_content"`  //问题内容
	ProblemAnswerA           string `bson:"problem_answer_a"` //答案A
	ProblemAnswerB           string `bson:"problem_answer_b"` //答案B
	ProblemRight             int32  `bson:"problem_right"`    //正确答案;0:A真确;1:B正确
	UpdateAt                 int64  `bson:"update_at"`
	CreatedAt                int64  `bson:"created_at"`
	CreatedAtString          string `bson:"create_at"`
}
