package mainbusinessenums

const (
	DatabaseGame                       = "beetu_%s"
	MongoLiveTableRoom                 = "live_room"
	MongoLiveTableRoomProblem          = "live_room_problem"
	MongoLiveTableRoomModel            = "live_room_model"
	MongoLiveTableRoomModelRound       = "live_room_model_round"
	MongoLiveTableBarrage              = "live_barrage"
	MongoLiveTableActivitiesUser       = "live_activities_user"
	MongoLiveTableActivitiesUserTwo    = "live_activities_user_two" //
	MongoLiveTableEndUserDetail        = "live_model_end_user_detail"
	MongoLiveTableEndCombinationDetail = "live_model_end_combination_detail"
	MongoLiveTableEndCombinationUser   = "live_model_end_combination_user"
	MongoLiveTableEndCombination       = "live_model_end_combination"
	MongoLiveTableUser                 = "live_user"
	MongoLiveTableUserLog              = "live_user_log"
	MongoLiveTableHost                 = "live_host"
	MongoLiveTableUserChangeMoney      = "live_user_change_money"

	CollectLiveConsumeError   = "live_consume_error"
	CollectLiveJobCommonError = "live_job_common_error"
)
