package petbattlemongoenum

type PetConfigGift struct {
}

const (
	PetConfigGiftStatusOnline  = 0
	PetConfigGiftStatusOffLine = 1
)

var PetConfigGiftObject PetConfigGift

func (e PetConfigGift) Status(status int) string {
	switch status {
	case PetConfigGiftStatusOnline:
		return "上线"
	case PetConfigGiftStatusOffLine:
		return "下线"
	default:
		return "未知"
	}
}
