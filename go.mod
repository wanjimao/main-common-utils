module gitee.com/wanjimao/main-common-utils

go 1.20

require (
	github.com/go-playground/locales v0.14.1
	github.com/go-playground/universal-translator v0.18.0
	github.com/google/uuid v1.3.0
	github.com/segmentio/kafka-go v0.4.38
	github.com/shopspring/decimal v1.3.1
	gopkg.in/go-playground/validator.v9 v9.31.0
	gorm.io/gorm v1.25.4
)

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/klauspost/compress v1.15.9 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/pierrec/lz4/v4 v4.1.15 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
)
